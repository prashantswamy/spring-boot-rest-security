
CREATE SEQUENCE testUserDbSeq START WITH 10 INCREMENT BY 1;

insert into TEST_USER (USER_ID, name, age, salary) values (1, 'Prashant Swamy', 32, 100);
insert into TEST_USER (USER_ID, name, age, salary) values (2, 'Narendra Modi', 62, 30000);
insert into TEST_USER (USER_ID, name, age, salary) values (3, 'Barak Obama', 52, 2000);
insert into TEST_USER (USER_ID, name, age, salary) values (4, 'Devendra Fadanvis', 49, 1000);
commit;