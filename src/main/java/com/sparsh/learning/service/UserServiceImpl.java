package com.sparsh.learning.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sparsh.learning.entity.UserEntity;
import com.sparsh.learning.exception.UserNotFoundException;
import com.sparsh.learning.model.UserModel;
import com.sparsh.learning.model.UserModelRequest;
import com.sparsh.learning.repository.UserRepository;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	private UserRepository userRepository;

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<UserModel> findAllUsers() {
		LOGGER.debug("findAllUsers method starts.");
		System.out.println("findAllUsers");

		Iterable<UserEntity> users = userRepository.findAll();
		List<UserModel> userModels = new ArrayList<UserModel>();
		if (users != null) {
			users.forEach(user -> {
				UserModel userModel = new UserModel(user.getUserId(), user.getName(), user.getAge(), user.getSalary());
				userModels.add(userModel);
			});
		}
		return userModels;

	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public UserModel findById(long userId) {
		LOGGER.debug("findById method starts.");

		UserModel userModelResponse = null;

		Optional<UserEntity> optional = userRepository.findById(userId);

		if (optional.isPresent()) {
			UserEntity userEntity = optional.get();
			if (Objects.nonNull(userEntity)) {
				userModelResponse = new UserModel(userEntity.getUserId(), userEntity.getName(), userEntity.getAge(),
						userEntity.getSalary());
			}
		}
		return userModelResponse;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public UserModel findByName(String name) {
		UserModel userModel = null;

		UserEntity user = userRepository.findByName(name);
		if (Objects.nonNull(user)) {
			userModel = new UserModel(user.getUserId(), user.getName(), user.getAge(), user.getSalary());
		}
		return userModel;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public UserModel saveUser(UserModelRequest user) {
		UserEntity userEntity = new UserEntity(user.getName(), user.getAge(), user.getSalary());
		userRepository.save(userEntity);
		return new UserModel(userEntity.getUserId(), userEntity.getName(), userEntity.getAge(), userEntity.getSalary());
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void updateUser(final long userId, final UserModelRequest userModelRequest) throws UserNotFoundException {

		Optional<UserEntity> optional = userRepository.findById(userId);

		if (optional.isPresent()) {
			UserEntity userEntity = optional.get();

			if (Objects.nonNull(userEntity)) {
				userEntity.setName(userModelRequest.getName());
				userEntity.setAge(userModelRequest.getAge());
				userEntity.setSalary(userModelRequest.getSalary());

				userRepository.save(userEntity);
			}
		} else {
			throw new UserNotFoundException();
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteUserById(long id) throws UserNotFoundException {
		userRepository.deleteById(id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public boolean isUserExist(UserModelRequest user) {
		boolean isUserExist = false;
		if (user != null && !user.getName().isEmpty()) {
			UserEntity found = userRepository.findByName(user.getName());
			if (found != null) {
				isUserExist = true;
			}
		}
		return isUserExist;
	}

	public void deleteAllUsers() {
		userRepository.deleteAll();
	}
}
