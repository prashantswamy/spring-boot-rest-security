package com.sparsh.learning.service;

import java.util.List;

import com.sparsh.learning.exception.UserNotFoundException;
import com.sparsh.learning.model.UserModel;
import com.sparsh.learning.model.UserModelRequest;

public interface UserService {

	List<UserModel> findAllUsers();

	UserModel findById(long id);

	UserModel findByName(String name);

	UserModel saveUser(UserModelRequest user);

	void updateUser(long userId, UserModelRequest user) throws UserNotFoundException;

	void deleteUserById(long id) throws UserNotFoundException;

	void deleteAllUsers();

	boolean isUserExist(UserModelRequest user);

}
