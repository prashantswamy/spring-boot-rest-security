package com.sparsh.learning;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Use the below swagger URL to access the APIs. http://localhost:8080/SpringBootRestSecurity/swagger-ui.html The
 * context path is now added in the application.yml file.
 * 
 * @author prashant.swamy
 */

@SpringBootApplication
@EnableSwagger2
public class Application implements WebMvcConfigurer {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	// Below 2 methods are for Swagger
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2) //
				.select() //
				.apis(RequestHandlerSelectors.basePackage("com.sparsh.learning")) // .apis(RequestHandlerSelectors.any())
				.paths(PathSelectors.any()) //
				.build() //
				.apiInfo(getApiInfo());

	}

	@SuppressWarnings("rawtypes")
	private ApiInfo getApiInfo() {
		String title = "Spring Boot Rest Application";
		String description = "Spring Boot Rest Application with Swagger";
		String version = "1.0";
		String termsOfServiceUrl = "Terms of Serice";
		Contact contact = new Contact("Prashant Swamy", "https://twitter.com/prashantswamy",
				"swamy.prashant@gmail.com");
		String license = "Apache License Version 2.0";
		String licenseUrl = "https://www.apache.org/licesen.html";
		Collection<VendorExtension> vendorExtensions = new ArrayList<VendorExtension>();
		// VendorExtension vendorExtension = new StringVendorExtension("VendorKey", "VendorValue");
		// vendorExtensions.add(vendorExtension);
		return new ApiInfo(title, description, version, termsOfServiceUrl, contact, license, licenseUrl,
				vendorExtensions);
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		// enabling swagger-ui part for visual documentation
		registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
		registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
	}
}
