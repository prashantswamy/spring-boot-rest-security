package com.sparsh.learning.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.sparsh.learning.entity.UserEntity;
import com.sparsh.learning.exception.UserNotFoundException;
import com.sparsh.learning.model.UserModel;
import com.sparsh.learning.model.UserModelRequest;
import com.sparsh.learning.model.UserModelResponse;
import com.sparsh.learning.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Controller
@Api(value = "UserRestController", description = "Operations pertaining to User Controller")
public class UserRestController {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserRestController.class);

	// Service which will do all data retrieval/manipulation work
	@Autowired
	private UserService userService;

	// Retrieve All Users
	@ApiOperation(value = "Get User List", response = ResponseEntity.class)
	@RequestMapping(value = "/users", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserModelResponse> listAllUsers() {
		LOGGER.debug("listAllUsers method starts.");

		List<UserModel> users = userService.findAllUsers();

		if (users.isEmpty()) {
			// You may decide to return HttpStatus.NOT_FOUND
			return new ResponseEntity<UserModelResponse>(HttpStatus.NO_CONTENT);
		}
		UserModelResponse userModelResponse = new UserModelResponse();
		userModelResponse.setUsers(users);

		return new ResponseEntity<UserModelResponse>(userModelResponse, HttpStatus.OK);
	}

	// Retrieve Single User
	@ApiOperation(value = "Get User For an Id")
	@RequestMapping(value = "/users/{id}/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserModel> getUser(@PathVariable("id") long id) {
		System.out.println("Fetching User with id " + id);
		UserModel userModel = userService.findById(id);
		if (userModel == null) {
			System.out.println("User with id " + id + " not found");
			return new ResponseEntity<UserModel>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<UserModel>(userModel, HttpStatus.OK);
	}

	// Create a User
	@RequestMapping(value = "/users", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> createUser(@RequestBody UserModelRequest user, UriComponentsBuilder ucBuilder) {
		LOGGER.debug("createUser method starts.");
		LOGGER.info("Creating User " + user.getName());

		if (userService.isUserExist(user)) {
			LOGGER.warn("A User with name " + user.getName() + " already exist");
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}

		UserModel userModel = userService.saveUser(user);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(userModel.getUserId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	// Update a User
	@RequestMapping(value = "/users/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> updateUser(@PathVariable("id") long id, @RequestBody UserModelRequest user)
			throws UserNotFoundException {
		LOGGER.debug("createUser method starts.");

		LOGGER.info("Updating User " + id);
		userService.updateUser(id, user);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	// Delete a User
	@RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteUser(@PathVariable("id") long id) throws UserNotFoundException {
		LOGGER.debug("deleteUser method starts.");

		try {
			LOGGER.info("Deleting User with id " + id);
			userService.deleteUserById(id);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (Exception e) {
			throw new UserNotFoundException("User not found for " + id);
		}
	}

	// Delete All Users
	@RequestMapping(value = "/users", method = RequestMethod.DELETE)
	public ResponseEntity<UserEntity> deleteAllUsers() {
		LOGGER.debug("deleteAllUsers method starts.");
		userService.deleteAllUsers();
		return new ResponseEntity<UserEntity>(HttpStatus.NO_CONTENT);
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService() {
		return userService;
	}

	/**
	 * @param userService the userService to set
	 */
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
}
