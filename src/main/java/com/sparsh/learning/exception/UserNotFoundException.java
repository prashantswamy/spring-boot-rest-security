package com.sparsh.learning.exception;

public class UserNotFoundException extends Exception {

	private static final long serialVersionUID = -5620630473657636136L;

	private String message;

	public UserNotFoundException() {
		super();
	}

	public UserNotFoundException(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UserNotFoundException [message=");
		builder.append(message);
		builder.append("]");
		return builder.toString();
	}
}
