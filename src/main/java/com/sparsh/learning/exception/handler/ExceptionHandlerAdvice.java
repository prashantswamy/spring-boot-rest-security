package com.sparsh.learning.exception.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sparsh.learning.exception.UserNotFoundException;
import com.sparsh.learning.model.ExceptionResponse;

@ControllerAdvice
public class ExceptionHandlerAdvice {

	@ExceptionHandler(UserNotFoundException.class)
	@ResponseBody
	public ResponseEntity<ExceptionResponse> handleUserNotFoundException(final UserNotFoundException exception) {
		ExceptionResponse exceptionResponse = new ExceptionResponse(exception.getMessage());
		return new ResponseEntity<ExceptionResponse>(exceptionResponse, HttpStatus.BAD_REQUEST);
	}
}
