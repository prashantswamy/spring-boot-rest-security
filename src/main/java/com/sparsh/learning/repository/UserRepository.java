package com.sparsh.learning.repository;

import org.springframework.data.repository.CrudRepository;

import com.sparsh.learning.entity.UserEntity;

// @RepositoryRestResource(collectionResourceRel = "user", path = "user")
public interface UserRepository extends CrudRepository<UserEntity, Long> {
	UserEntity findByName(String name);
}
