package com.sparsh.learning.model;

public class ExceptionResponse {

	private String message;

	public ExceptionResponse() {
		super();
	}

	public ExceptionResponse(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ExceptionResponse [message=");
		builder.append(message);
		builder.append("]");
		return builder.toString();
	}
}
